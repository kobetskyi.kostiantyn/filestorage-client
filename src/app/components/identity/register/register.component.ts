import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {PasswordConfirmService} from "../../../services/password-confirm.service";
import {UserCredentials} from "../../../models/user-credentials";
import {IdentityService} from "../../../services/identity.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form: FormGroup
  emailServerErrors: string[] = []
  passwordServerErrors: string[] = []
  errors: string[] = []

  constructor(private fb: FormBuilder,
              private passMatch: PasswordConfirmService,
              private router: Router,
              private identityService: IdentityService) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
        email: new FormControl('', [Validators.required, Validators.email]),
        password: new FormControl('', Validators.required),
        confirmPassword: new FormControl('', Validators.required)
      },
      {validators: this.passMatch.validate('password', 'confirmPassword')})
  }

  getEmailErrorMessage() {
    let field = this.form.get('email')
    if (field?.hasError('required')) {
      return 'Email field is required'
    }

    if (field?.hasError('email')) {
      return 'Email field is invalid'
    }

    if (field?.hasError('emailServerError')) {

      let errormessage = ''
      this.emailServerErrors.forEach(err => errormessage += err)
      return errormessage
    }

    return ''
  }

  getConfirmPasswordErrors() {
    let field = this.form.get('confirmPassword')
    if (field?.hasError('required')) {
      return 'Confirm password field is required'
    }
    if (field?.hasError('passwordMismatch')) {
      return "Passwords doesn't match"
    }
    return ''
  }

  getData(): UserCredentials {
    let email = this.form.value['email']
    let pass = this.form.value['password']
    return {email: email, password: pass}
  }

  processErrors(error: HttpErrorResponse) {
    if (error) {
      error.error.forEach((val: any) => {
        if (val.code !== 'DuplicateUserName') {
          if (val.description.toLowerCase().includes('email')) {
            this.emailServerErrors.push(val.description)
            this.form.get('email')?.setErrors({emailServerError: true})
          } else if (val.description.toLowerCase().includes('password')) {
            this.passwordServerErrors.push(val.description)
            this.form.get('password')?.setErrors({passwordServerError: true})
          } else {
            this.errors.push(val.description)
          }
        }
      })
    }
  }

  register(registerModel: UserCredentials) {
    this.clearErrors()
    this.identityService.register(registerModel).subscribe(response => {
      this.identityService.saveToken(response)
      this.router.navigate(['/'])
      // console.log(response)
      }, error => {
        console.log(error)
        this.processErrors(error)
      }
    )
  }

  clearErrors() {
    this.emailServerErrors= []
    this.passwordServerErrors = []
    this.errors = []
  }
}
