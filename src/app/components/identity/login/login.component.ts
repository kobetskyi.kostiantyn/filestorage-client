import {Component, OnInit} from '@angular/core';
import {UserCredentials} from "../../../models/user-credentials";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {IdentityService} from "../../../services/identity.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../register/register.component.css']
})
export class LoginComponent implements OnInit {

  error: string =''
  form: FormGroup

  constructor(private fb: FormBuilder,
              private identityService: IdentityService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required)
    })
  }

  login(value: UserCredentials) {
    this.error = ''
    this.identityService.login(value)
      .subscribe(response=>{
        this.identityService.saveToken(response)
        this.router.navigate(['/'])
      }, error=>{
        if (error){
          console.log(error)
          this.error = 'Incorrect login or password'
        }
      })
  }

  getEmailErrorMessage() {
    let field = this.form.get('email')
    if (field?.hasError('required')) {
      return 'Email field is required'
    }

    if (field?.hasError('email')) {
      return 'Email field is invalid'
    }
    return ''
  }
}
