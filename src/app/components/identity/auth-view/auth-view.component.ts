import {Component, Input, OnInit} from '@angular/core';
import {IdentityService} from "../../../services/identity.service";

@Component({
  selector: 'app-auth-view',
  templateUrl: './auth-view.component.html',
  styleUrls: ['./auth-view.component.css']
})
export class AuthViewComponent implements OnInit {

  constructor(private identityService: IdentityService) {
  }

  ngOnInit(): void {
  }

  @Input()
  role: string

  public isAuthorized() {
    if (this.role) {
      return this.identityService.getRole() ===this.role
    } else {
      return this.identityService.isAuthenticated()
    }
  }

}
