import {Component, OnInit, ViewChild} from '@angular/core';
import {UserFilesService} from "../../services/userFiles.service";
import {FileInfo} from "../../models/file-info";
import {TableComponent} from "../userFiles/table/table.component";
import {IdentityService} from "../../services/identity.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  file: FileInfo
  @ViewChild(TableComponent, {static:false}) tableComp:TableComponent
  authorized:boolean

  constructor(private fileService: UserFilesService, private identityService: IdentityService) {
    this.authorized = this.identityService.isAuthenticated()
  }

  ngOnInit(): void {
  }

  getFile() {
    // this.fileService.get(id).subscribe(
    //   response=>{
    //   this.file = <FileInfo>Object.values(response)[0]
      this.tableComp.getData()
    // })
  }
}
