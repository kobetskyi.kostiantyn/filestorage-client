import { Component, OnInit } from '@angular/core';
import {IdentityService} from "../../services/identity.service";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private identityService: IdentityService) { }

  userEmail:string =''

  ngOnInit(): void {

  }

  getEmail():string{
    return this.identityService.userEmail
  }

  logOut() {
    this.identityService.logOut()
  }
}
