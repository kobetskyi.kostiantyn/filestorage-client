import {Component, OnInit, ViewChild} from '@angular/core';
import {UserFilesService} from "../../../services/userFiles.service";
import {HttpEventType, HttpResponse} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";
import {FileInfo} from "../../../models/file-info";

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.css']
})
export class DownloadComponent implements OnInit {

  shortLink:string
  file: FileInfo
  dataFetched:boolean = false


  constructor(private filesService: UserFilesService,
              private route:ActivatedRoute) {
  }

  ngOnInit(): void {
    this.getRouteParam()
    this.fetchFileInfo()
  }

  download() {
      this.filesService.download(this.file.id).subscribe((response: any) => {
      let dataType = response.type
      let binaryData = []
      binaryData.push(response)
      const a = document.createElement('a')
      const objectUrl = URL.createObjectURL(new Blob(binaryData, {type: dataType}))

      a.href = objectUrl

      a.download = this.file.name
      a.click()
      URL.revokeObjectURL(objectUrl)

    }), (error: any) => console.log('Error downloading the file'+ error), //when you use stricter type checking
      () => console.info('File downloaded successfully')
  }

  getRouteParam() {
    this.route.params.subscribe(params=>{
        this.shortLink = params.link
    })
  }

  fetchFileInfo() {
    this.filesService.getByShortLink(this.shortLink).subscribe(response=>{
      this.file=(response)
      this.dataFetched = true
    }, error => {
      this.dataFetched = true
    })
  }
}
