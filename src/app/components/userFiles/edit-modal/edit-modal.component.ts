import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {EditModel} from "../../../models/editModel";

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.css']
})
export class EditModalComponent implements OnInit {
  form: FormGroup;

  constructor(private fb: FormBuilder,
    public dialogRef: MatDialogRef<EditModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: EditModel) { }

  ngOnInit(): void {
    this.extractExtension()
    this.initForm()
  }
  extension: string
  fileName: string

  response:EditModel


  initForm() {
    this.form = this.fb.group({
      name:[this.fileName, [Validators.required, Validators.maxLength(200)]],
      status:[this.data.status]
    })
  }

  extractExtension() {
   this.fileName = this.data.name.substring(0, this.data.name.lastIndexOf('.'));
   let extensionLength = this.data.name.length - this.fileName.length
   this.extension = this.data.name.substring(this.data.name.length - extensionLength)
  }

  confirm(value:EditModel) {
    value.name +=this.extension
    this.dialogRef.close({ data: value })
  }

  getNameErrorMessage():string{
    let field = this.form.get('name')
    if (field?.hasError('required')){
      return 'Name field is required'
    }
    if (field?.hasError('maxlength')){
      return 'New name is too long (max 200 symbols)'
    }
    return ''
  }

}
