import {Component, EventEmitter, Output} from '@angular/core';
import {HttpEventType} from "@angular/common/http";
import {UserFilesService} from "../../../services/userFiles.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Clipboard} from "@angular/cdk/clipboard";
import {FileInfo} from "../../../models/file-info";
import {Router} from "@angular/router";

const baseUrl = 'http://localhost:4200/'

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent {
  message: string = ''
  progress: number = 0
  fileUploading: boolean = false
  UploadFinished: boolean = false

  @Output() public onUploadFinished = new EventEmitter<any>()

  constructor(private filesService: UserFilesService, private snackBar: MatSnackBar, private clipboard: Clipboard,
              private fileService: UserFilesService,
              private router: Router) {
  }

  public uploadFile = (files: any) => {
    this.UploadFinished = false
    if (files.length === 0)
      return

    let fileToUpload = <File>files[0]
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name)

    this.filesService.upload(formData)
      .subscribe(e => {
        if (e.type === HttpEventType.UploadProgress) {
          this.fileUploading = true
          this.progress = Math.round(100 * (e.loaded / e.total))
        } else if (e.status === 400) {
          this.processFailUpload()
        } else if (e.type === HttpEventType.Response) {
          this.processSuccessUpload(e)
        }
      }, error => {
        this.processFailUpload()
      })
  }

  processSuccessUpload(e: any) {
    this.UploadFinished = true
    this.fileUploading = false
    let userAuthorized = localStorage.getItem('jwt')
    if (userAuthorized) {
      this.snackBar.open('Upload success', '', {duration: 1000})
      this.onUploadFinished.emit()
    } else {
      this.fileService.get(e.body.id).subscribe(resp => {
        const file = <FileInfo>Object.values(resp)[0]

        const link = `${baseUrl}${file.link}`
        console.log(link)

        let copyResult = false
        while (!copyResult) {
          copyResult = this.clipboard.copy(link)
          if (!copyResult){
            console.log('Copy operation failed')
          }
        }


        this.snackBar.open('Upload success. Link copied to clipboard', '', {duration: 3000})
      })

    }
  }

  processFailUpload() {
    this.UploadFinished = false
    this.fileUploading = false
    this.snackBar.open('Upload failed', '', {duration: 2000})
  }
}
