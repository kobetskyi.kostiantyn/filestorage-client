import {Component, OnInit} from '@angular/core';
import {UserFilesService} from "../../../services/userFiles.service";
import {ActivatedRoute} from "@angular/router";
import {FileInfo} from "../../../models/file-info";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {FileStatus} from "../../../models/fileStatus";

@Component({
  selector: 'app-details',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  fileId:number
  file: FileInfo
  fetching: boolean = false
  form: FormGroup
  statuses: FileStatus[]
  selectedStatus: number
  downloaded: boolean = false


  constructor(private fileService: UserFilesService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.getRouteParam()
    this.fetchFileInfo()

  }

  private getRouteParam() {
    this.route.params.subscribe(params=>{
      this.fileId = params.id
    })
  }

  private fetchFileInfo() {
    this.downloaded = false
    this.fetching = true
    this.fileService.get(this.fileId).subscribe(response=>{
      this.file = <FileInfo>Object.values(response)[0]
      this.statuses = <FileStatus[]>Object.values(response)[1]
      this.selectedStatus = this.file.statusId
      this.downloaded = true
      this.fetching = false
    }, error => {
      this.fetching =false
      this.downloaded = true
    })

  }

  saveFile(value:any) {

    console.log(value)
  }
}
