import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {FileInfo} from "../../../models/file-info";
import {UserFilesService} from "../../../services/userFiles.service";
import {Clipboard} from "@angular/cdk/clipboard";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {DeleteModalComponent} from "../delete-modal/delete-modal.component";
import {EditModalComponent} from "../edit-modal/edit-modal.component";
import {EditModel} from "../../../models/editModel";
import {FileStatus} from "../../../models/fileStatus";
import {PageEvent} from "@angular/material/paginator";

const baseUrl = 'http://localhost:4200/'

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit{


  constructor(private filesService: UserFilesService,
              private clipboard: Clipboard,
              private dialog: MatDialog) {
  }


  totalFilesCount: number
  currentPage: number = 1
  pageSize: number = 5
  statusList: FileStatus[]
  displayedColumns: string[] = ['position', 'name', 'fileSize', 'date', 'status', 'actions']
  dataSource: MatTableDataSource<FileInfo>
  @ViewChild(MatTable) table: MatTable<any>

  ngOnInit(): void {

    this.getData()
    this.setFilterOptions()
  }

  getData() {
    this.filesService.getAll(this.currentPage, this.pageSize).subscribe(response => {
      this.statusList = <FileStatus[]>Object.values(response)[2]
      this.dataSource = new MatTableDataSource(<FileInfo[]>Object.values(response)[1])
      this.totalFilesCount = <number>Object.values(response)[0]
    })

  }

  setFilterOptions() {
    this.dataSource.filterPredicate = function (data, filter) {
      return data.name == filter
    }
  }


  copyToClipboard(link: string) {
    this.clipboard.copy(`${baseUrl}${link}`)
  }

  deleteFile(id: number) {
    this.filesService.delete(id).subscribe(result => {
      console.log('File deleted')
    this.getData()
    })
  }


  openDeleteDialog(file: FileInfo) {
    const id = file.id
    const dialogRef = this.dialog.open(DeleteModalComponent, {
      width: '500px',
      data: file
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteFile(id)
      }
    })
  }

  openEditDialog(file: FileInfo) {

    let fileStatus = this.statusList.find(x => x.id === file.statusId)!
    let model: EditModel = {name: file.name, statuses: this.statusList, status: fileStatus}
    const config = new MatDialogConfig()
    config.width = '500px'
    config.data = model

    const dialogRef = this.dialog.open(EditModalComponent, config);


    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        file.name = result.data.name
        file.statusId = result.data.status.id
        this.filesService.update(file).subscribe(resp => {
          this.getData()
        })
      }
    });
  }

  getStatusName(statusId: number) {
    return this.statusList.find(s => s.id === statusId)!.value
  }

  download(file: FileInfo) {
    this.filesService.download(file.id).subscribe((response: any) => {
      let dataType = response.type
      let binaryData = []
      binaryData.push(response)
      const a = document.createElement('a')
      const objectUrl = URL.createObjectURL(new Blob(binaryData, {type: dataType}))

      a.href = objectUrl

      a.download = file.name
      a.click()
      URL.revokeObjectURL(objectUrl)

    }), (error: any) => console.log('Error downloading the file' + error),
      () => console.info('File downloaded successfully')
  }

  updatePagination(event: PageEvent) {
    this.currentPage = event.pageIndex + 1
    this.pageSize = event.pageSize
    this.getData()
  }

  applyFilter(value: string) {
    value = value.trim()
    value = value.toLowerCase()
    this.dataSource.filter = value;

  }


}
