import {Injectable, Input} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UserCredentials} from "../models/user-credentials";
import {Observable} from "rxjs";
import {JwtHelperService} from "@auth0/angular-jwt";


const baseUrl = 'https://localhost:5001/api/Accounts'


@Injectable({
  providedIn: 'root'
})
export class IdentityService {
  constructor(private http: HttpClient,
              private jwtHelper: JwtHelperService) {
  }

  userRole: string = ''
  userEmail: string = ''

  isAuthenticated(): boolean {
    let token = localStorage.getItem('jwt')
    if (!token)
      return false

    if (this.jwtHelper.isTokenExpired(token)) {
      this.logOut()
      return false
    }
    return true
  }

  getToken() {
    return localStorage.getItem('jwt')
  }

  getTokenData() {
    let token = localStorage.getItem('jwt')
    if (token) {
      let tokenData = Object.values(this.jwtHelper.decodeToken(token))
      this.userEmail = <string>tokenData[0]
      this.userRole = <string>tokenData[1]

      console.log(this.userEmail + ' ' + this.userRole)
    }
  }

  getRole(): string {
    return this.userRole
  }


  logOut() {
    this.userRole = ''
    this.userEmail = ''
    localStorage.removeItem('jwt')
  }

  register(model: UserCredentials): Observable<any> {
    return this.http.post<any>(`${baseUrl}/register`, model)
  }

  login(model: UserCredentials): Observable<any> {
    return this.http.post<any>(`${baseUrl}/login`, model)
  }

  saveToken(token: any) {
    localStorage.setItem('jwt', token.token)
    this.getTokenData()
  }
}
