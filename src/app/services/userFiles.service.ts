import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient, HttpParams, HttpResponse} from "@angular/common/http";
import {FileInfo} from "../models/file-info";
import {FileStatus} from "../models/fileStatus";

const baseUrl = 'https://localhost:5001/api/UserFiles'

@Injectable({
  providedIn: 'root'
})
export class UserFilesService {

  constructor(private http:HttpClient) { }

  getAll(pageNumber: number, pageSize:number):Observable<[FileInfo[], FileStatus[], number]>{
    let params = new HttpParams()
    params = params.append('pageNumber', pageNumber.toString())
    params = params.append('pageSize', pageSize.toString())
    return this.http.get<[FileInfo[], FileStatus[], number]>(baseUrl, {params})
  }

  get(id:any):Observable<[FileInfo, FileStatus[]]>{
    return this.http.get<[FileInfo, FileStatus[]]>(`${baseUrl}/${id}`)
  }

  getByShortLink(id:string):Observable<FileInfo>{
    return this.http.get<FileInfo>(`${baseUrl}/link/${id}`)
  }

  upload(formData: any): Observable<any>{
    return this.http.post(`${baseUrl}/upload`, formData, {reportProgress:true, observe:'events'});
  }

  download(id: number){
    return this.http.get(`${baseUrl}/download/${id}`, {responseType: 'blob' as 'json'})
  }

  update(fileInfo:FileInfo):Observable<any>{
    return this.http.put(baseUrl, fileInfo)
  }
  delete(id:any):Observable<any>{
    return this.http.delete(`${baseUrl}/${id}`)
  }
}
