import {FileStatus} from "./fileStatus";

export interface FileInfo {
  id: number
  name: string
  changed: string
  fileSize: number
  statusId: number
  link: string
}
