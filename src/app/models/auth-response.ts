export interface AuthResponseModel {
  token: string
  exp: Date
}
