import {FileStatus} from "./fileStatus";

export interface EditModel {
  name: string
  status: FileStatus
  statuses: FileStatus[]
}
