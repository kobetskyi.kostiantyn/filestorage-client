import { Pipe, PipeTransform } from '@angular/core';

export type ByteUnit = 'B' | 'kB' | 'KB' | 'MB' | 'GB' | 'TB';

@Pipe({
  name: 'convertBytes'
})



export class ConvertBytesPipe implements PipeTransform {

  static formats: { [key: string]: { max: number; prev?: ByteUnit } } = {
    B: {max: 1024},
    kB: {max: Math.pow(1024, 2), prev: 'B'},
    KB: {max: Math.pow(1024, 2), prev: 'B'},
    MB: {max: Math.pow(1024, 3), prev: 'kB'},
    GB: {max: Math.pow(1024, 4), prev: 'MB'},
    TB: {max: Number.MAX_SAFE_INTEGER, prev: 'GB'},
  };


  transform(input: number, decimal: number = 0): any {
    if (input < 1024) {
      return `${input}`;
    }

    for (const key in ConvertBytesPipe.formats) {
      if (ConvertBytesPipe.formats.hasOwnProperty(key)) {
        const format = ConvertBytesPipe.formats[key];
        if (input < format.max) {
          const result = this.toDecimal(ConvertBytesPipe.calculateResult(format, input), decimal);

          return ConvertBytesPipe.formatResult(result, key);
        }
      }
    }


  }

 toDecimal(value: number, decimal: number): number {
    return Math.round(value * Math.pow(10, decimal)) / Math.pow(10, decimal);
  }

  static formatResult(result: number, unit: string): string {
    return `${result} ${unit}`;
  }

  static calculateResult(format: { max: number; prev?: ByteUnit }, bytes: number) {
    const prev = format.prev ? ConvertBytesPipe.formats[format.prev] : undefined;
    return prev ? bytes / prev.max : bytes;
  }



}
