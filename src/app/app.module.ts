import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {UploadComponent} from './components/userFiles/upload/upload.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MenuComponent} from './components/menu/menu.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {HomeComponent} from './components/home/home.component';
import {TableComponent} from './components/userFiles/table/table.component';
import {DownloadComponent} from './components/userFiles/download/download.component';
import {EditComponent} from './components/userFiles/details/edit.component';
import {MatTableDataSource, MatTableModule} from "@angular/material/table";
import {MatCardModule} from "@angular/material/card";
import {ConvertBytesPipe} from './pipes/convert-bytes.pipe';
import {AuthViewComponent} from './components/identity/auth-view/auth-view.component';
import {AdminAreaComponent} from './components/admin/admin-area/admin-area.component';
import {LoginComponent} from './components/identity/login/login.component';
import {RegisterComponent} from './components/identity/register/register.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {JwtModule} from "@auth0/angular-jwt";
import {JwtInterceptorInterceptor} from "./services/jwt-interceptor.interceptor";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import { DeleteModalComponent } from './components/userFiles/delete-modal/delete-modal.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import { CutExtensionPipe } from './pipes/cut-extension.pipe';
import {MatRadioModule} from "@angular/material/radio";
import {MatDividerModule} from "@angular/material/divider";
import { EditModalComponent } from './components/userFiles/edit-modal/edit-modal.component';
import {MatSelectModule} from "@angular/material/select";
import {MatMenuModule} from "@angular/material/menu";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatListModule} from "@angular/material/list";


export function tokenGetter() {
  return localStorage.getItem("jwt")
}

@NgModule({
  declarations: [
    AppComponent,
    UploadComponent,
    MenuComponent,
    HomeComponent,
    TableComponent,
    DownloadComponent,
    EditComponent,
    ConvertBytesPipe,
    AuthViewComponent,
    AdminAreaComponent,
    LoginComponent,
    RegisterComponent,
    DeleteModalComponent,
    CutExtensionPipe,
    EditModalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatTableModule,
    MatCardModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatDividerModule,
    MatRadioModule,
    MatMenuModule,
    MatListModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ['localhost:5001'],
        disallowedRoutes: []
      }
    }),
    MatDialogModule,
    MatSelectModule,
    MatPaginatorModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: JwtInterceptorInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
