import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./components/home/home.component";
import {DownloadComponent} from "./components/userFiles/download/download.component";
import {EditComponent} from "./components/userFiles/details/edit.component";
import {AdminAreaComponent} from "./components/admin/admin-area/admin-area.component";
import {IsAdminGuard} from "./guards/is-admin.guard";
import {LoginComponent} from "./components/identity/login/login.component";
import {RegisterComponent} from "./components/identity/register/register.component";
import {IsUserGuard} from "./guards/is-user.guard";

const routes: Routes = [
  {path: '', component:HomeComponent},
  {path: 'details/:id', component:EditComponent, canActivate:[IsUserGuard]},
  {path: 'admin', component:AdminAreaComponent, canActivate:[IsAdminGuard]},
  {path: 'login', component:LoginComponent},
  {path: 'register', component:RegisterComponent},
  {path: ':link', component:DownloadComponent},
  {path: '**', redirectTo:''}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
